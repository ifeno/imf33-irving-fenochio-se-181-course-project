Irving Fenochio

SE181

10 July 2021

  
READ ME FILE:

The main concern I wish to express in this read me file if it was correct to end with generic banking.Account class which has banking.Savings, banking.Checking, and banking.CD as subtypes. I left my orignal indiviual class files for banking.Savings and banking.Checking. I had not yet created a banking.CD one as by then I had the banking.Account classes ready.

Below is my thought process which led to me creating the banking.Account class.

----------------------------------------------------------------------------

Hello. So I am writing this to explain my thought process for going thorough creating the classes and how I ended up with a unified banking.Account class.

Mainly I used lab 2 as a starting point for implementing my first account type. I chose to start with a banking.Savings. I after had gone through creating test and ensuring they work I started the container for the accounts a banking.Bank class.

I continued using lab 2 to develop my bank class. After going throught the TDD process, I reached the point to implement another account type. I added the checking. At this point I realized the banking.Bank class used a Map specific to only savings. Taking example from Assigment 1, I began testing for a generic banking.Account class with strings to distinguish account types. I rewrote the test for the banking.Bank to work with the banking.Account class.

I left my original test for each class. As this assignment has us only implement the same functions for each the banking.Account test are the same to the ones in the original banking.Savings and banking.Checking Test classes.