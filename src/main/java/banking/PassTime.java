package banking;

import java.util.HashSet;
import java.util.Set;

public class PassTime {
    Bank bank;
    public PassTime(Bank bank) {
        this.bank = bank;
    }

    public void pass(int months) {
        for (int t = 1; t <= months; t++){
            Set<String> accountsList = new HashSet<>(bank.getAccounts().keySet());

            for (String acctID : accountsList){
                Account account = bank.getAcctFromBank(acctID);
                account.passMonth();

                if (account.getBalance() == 0){
                    bank.getAccounts().remove(acctID);
                }
                else if (account.getBalance() < 100){
                    bank.chargeMinimumBalanceFee(acctID);
                }

                bank.calculateAPR(account);
            }
        }
    }
}
