package banking;

import java.util.Locale;

public class CommandValidator {
    Bank bank;
    CreateComValidator createValidator;
    DepositComValidator depositValidator;
    WithdrawComValidator withdrawValidator;
    TransferComValidator transferValidator;
    PassTimeComValidator passtimeValidator;

    public CommandValidator(Bank bank) {
        this.bank = bank;
        this.createValidator = new CreateComValidator(this.bank);
        this.depositValidator = new DepositComValidator(this.bank);
        this.withdrawValidator = new WithdrawComValidator(this.bank);
        this.transferValidator = new TransferComValidator(this.bank, this.depositValidator, this.withdrawValidator);
        this.passtimeValidator = new PassTimeComValidator(this.bank);
    }

    private String[] splitCommand(String command){
        command = command.stripTrailing();
        command = command.toLowerCase(Locale.ROOT);
        return command.split(" ");
    }

    public boolean validate(String command) {
        String[] subCommands = splitCommand(command);

        switch (subCommands[0]) {
            case "create":
                return createValidator.validate(subCommands);

            case "deposit":
                return depositValidator.validate(subCommands);

            case "withdraw":
                return withdrawValidator.validate(subCommands);

            case "transfer":
                return transferValidator.validate(subCommands);

            case "pass":
                return passtimeValidator.validate(subCommands);

            default:
                return false;

        }


    }

}



