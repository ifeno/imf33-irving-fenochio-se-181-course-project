package banking;

public class PassTimeComValidator extends Validator{

    public PassTimeComValidator(Bank bank) {
        super(bank);
    }

    private boolean verifyTimeTerm(String monthsStr) {
        try {
            int months = Integer.parseInt(monthsStr);
            return months >= 1 && months <= 60 ;
        } catch (NumberFormatException nfe){
            return false;
        }
    }

    public boolean validate(String[] subCommands) {
        return requiredComTerms(subCommands.length, 2, 2) &&
                verifyTimeTerm(subCommands[1]);
    }
}
