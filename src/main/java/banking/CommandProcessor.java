package banking;

import java.util.Locale;

public class CommandProcessor {
    private Bank bank;
    private CreateComProcessor createProcessor;
    private DepositComProcessor depositProcessor;
    private WithdrawComProcessor withdrawProcessor;
    private TransferComProcessor transferProcessor;
    private PassTimeComProcessor passtimeProcessor;


    public CommandProcessor(Bank bank) {
        this.bank = bank;
        this.createProcessor = new CreateComProcessor(this.bank);
        this.depositProcessor = new DepositComProcessor(this.bank);
        this.withdrawProcessor = new WithdrawComProcessor(this.bank);
        this.transferProcessor = new TransferComProcessor(this.bank);
        this.passtimeProcessor = new PassTimeComProcessor(this.bank);

    }

    private String[] splitCommand(String command){
        command = command.toLowerCase(Locale.ROOT);
        return command.split(" ");
    }

    public void processCommand(String command) {
        String[] subCommands = splitCommand(command);

        switch (subCommands[0]) {
            case "create":
                createProcessor.processCommand(subCommands);
                break;

            case "deposit":
                depositProcessor.processCommand(subCommands);
                break;

            case "withdraw":
                withdrawProcessor.processCommand(subCommands);
                break;

            case "transfer":
                transferProcessor.processCommand(subCommands);
                break;

            case "pass":
                passtimeProcessor.processCommand(subCommands);
                break;

            default:
                break;
        }
    }
}
