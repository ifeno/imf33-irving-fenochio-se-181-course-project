package banking;

import static java.lang.Math.round;

public abstract class CommandTestingPackage {
    protected static final String QUICK_ID = "12345678";
    protected static final String EXIST_ID = "00000000";
    protected static double RATE = 0.6;
    protected String[] subCommands;
    protected String command;
    protected Bank bank;
    protected boolean actual;

    protected String[] splitCommandIntoTerms(String command) {
        return command.split(" ");
    }

    double round2dec(double amount){
        amount *= 100;
        amount = round(amount);
        return amount / 100;
    }
}
