package banking;

public class WithdrawComValidator extends Validator {

    public WithdrawComValidator(Bank bank) {
        super(bank);
    }

    private boolean amountIsValidForAcct(String acctID, String amountStr) {
        double amount = Double.parseDouble(amountStr);
        double maxWithdraw = getAcctFromBank(acctID).getMaxWithdraw();
        return 0 <= amount && amount <= maxWithdraw;
    }

    private boolean verifyWithdrawLimit(String acctID) {
        if (getAcctFromBank(acctID).getAcctType().matches("savings")) {
            int withdrawsThisMonth = getAcctFromBank(acctID).getWithdrawsThisMonth();
            return withdrawsThisMonth == 0;
        } else {
            return true;
        }
    }

    private boolean verifyCdStatus(String acctID, String amount) {
        if (getAcctFromBank(acctID).getAcctType().matches("cd")) {
            return verifyCdAge(acctID) && verifyCdAmount(acctID, Double.parseDouble(amount));
        } else {
            return true;
        }
    }

    private boolean verifyCdAge(String acctID) {
        int age = getAcctFromBank(acctID).getAcctAge();
        return age >= 12;
    }

    private boolean verifyCdAmount(String acctID, double amount) {
        double balance = getAcctFromBank(acctID).getBalance();
        return amount >= balance;
    }

    protected boolean validate(String[] subCommands) {
        return requiredComTerms(subCommands.length, 3, 3) &&
                idExistInBank(subCommands[1]) &&
                termIsNumericalToDouble(subCommands[2]) &&
                amountIsValidForAcct(subCommands[1], subCommands[2]) &&
                verifyWithdrawLimit(subCommands[1]) &&
                verifyCdStatus(subCommands[1], subCommands[2]);
    }

}
