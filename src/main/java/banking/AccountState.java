package banking;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class AccountState {

    public String getAccountStatus(Account account) {
        DecimalFormat decFormat = new DecimalFormat("0.00");
        decFormat.setRoundingMode(RoundingMode.FLOOR);

        String acctType = account.getAcctType();
        acctType = acctType.substring(0, 1).toUpperCase() + acctType.substring(1);
        String balance = decFormat.format(account.getBalance());
        String APR = decFormat.format(account.getRate());

        return String.format("%s %s %s %s",acctType, account.getId(), balance, APR);
    }
}
