package banking;

public class Savings extends Account {

    public Savings(String id) {
        super("savings", id);
        setMaxDeposit(2500);
        setMaxWithdraw(1000);
    }


}
