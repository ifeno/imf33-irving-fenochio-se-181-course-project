package banking;

public class DepositComValidator extends Validator {

    public DepositComValidator(Bank bank) {
        super(bank);
    }

    private boolean verifyDepositAmmount(String amountStr, String acctID) {
            double amount = Double.parseDouble(amountStr);
            double max = bank.getAccounts().get(acctID).getMaxDeposit();
            return 0 <= amount && amount <= max ;
    }

    public boolean validate(String[] subCommands) {
        return requiredComTerms(subCommands.length, 3, 3) &&
                subCommands[0].matches("deposit") &&
                idExistInBank(subCommands[1]) &&
                termIsNumericalToDouble(subCommands[2]) &&
                verifyDepositAmmount(subCommands[2], subCommands[1]);
    }
}
