package banking;

public class DepositComProcessor extends Processor {

    public DepositComProcessor(Bank bank) {
        super(bank);
    }

    public void processCommand(String[] subCommands) {
        bank.depositToAcct(subCommands[1], Double.parseDouble(subCommands[2]));
    }
}
