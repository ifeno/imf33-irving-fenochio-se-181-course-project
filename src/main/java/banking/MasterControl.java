package banking;

import java.util.List;

public class MasterControl {
    Bank bank;
    CommandStorage comStorage;
    CommandValidator comValidator;
    CommandProcessor comProcessor;

    public MasterControl(Bank b, CommandStorage s, CommandValidator v, CommandProcessor p) {
        this.bank = b;
        this.comStorage = s;
        this.comValidator = v;
        this.comProcessor = p;
    }

    public List<String> start(List<String> inputs) {
        for (String command : inputs) {
            if (comValidator.validate(command)) {
                comProcessor.processCommand(command);
                comStorage.logTransaction(command);
            } else {
                comStorage.addInvalidCommand(command);
            }
        }
        return comStorage.getCommandsList();
    }
}
