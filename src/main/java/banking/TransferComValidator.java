package banking;

public class TransferComValidator extends Validator {
    protected DepositComValidator depositComValidator;
    protected WithdrawComValidator withdrawComValidator;

    public TransferComValidator(Bank bank, DepositComValidator d, WithdrawComValidator w) {
        super(bank);
        this.depositComValidator = d;
        this.withdrawComValidator = w;
    }

    private boolean acctNotCD(String acctID) {
        return !bank.getAcctFromBank(acctID).getAcctType().matches("cd");
    }

    private boolean isSameAcct(String fromID, String toID) {
        return fromID.equals(toID);
    }

    private boolean verifyIDs(String fromID, String toID) {
        return !isSameAcct(toID, fromID) &&
                idExistInBank(fromID) &&
                idExistInBank(toID) &&
                acctNotCD(fromID) && acctNotCD(toID);
    }

    private boolean validateWithdrawFromId(String acctID, String amount) {
        String[] subCommands = String.format("withdraw %s %s",acctID,amount).split(" ");
        return withdrawComValidator.validate(subCommands);
    }

    private boolean validateDepositToId(String toID, String amountStr) {
        String[] subCommands = String.format("deposit %s %s", toID, amountStr).split(" ");
        return depositComValidator.validate(subCommands);
    }

    public boolean validate(String[] subCommands) {
        return requiredComTerms(subCommands.length, 4, 4) &&
                verifyIDs(subCommands[1],subCommands[2]) &&
                termIsNumericalToDouble(subCommands[3]) &&
                validateWithdrawFromId(subCommands[1], subCommands[3]) &&
                validateDepositToId(subCommands[2], subCommands[3]);
    }
}
