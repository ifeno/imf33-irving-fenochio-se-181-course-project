package banking;

import java.util.HashMap;
import java.util.Map;

public class Bank {
    public APRCalculator aprCalculator;
    private Map<String, Account> accounts;
    private AccountState accountState;

    Bank() {
        accounts = new HashMap<>();
        aprCalculator = new APRCalculator();
        accountState = new AccountState();
    }

    protected Map<String, Account> getAccounts() {
        return accounts;
    }

    protected void addSavings(String quickId, double rate) {
        accounts.put(quickId, new Savings(quickId));
        accounts.get(quickId).newRate(rate);
    }

    protected void addChecking(String quickId, double rate) {
        accounts.put(quickId, new Checking(quickId));
        accounts.get(quickId).newRate(rate);
    }

    protected void addCD(String quickId, double rate, double amount) {
        accounts.put(quickId, new CD(quickId, rate, amount));
    }

    protected Account getAcctFromBank(String acctID) {
        return getAccounts().get(acctID);
    }

    protected void depositToAcct(String quickId, double amount) {
        accounts.get(quickId).deposit(amount);
    }

    protected double withdrawFromAcct(String quickId, double amount) {
        accounts.get(quickId).increaseNumWithdraws();
        return accounts.get(quickId).withdraw(amount);
    }

    protected boolean accountExistByQuickID(String idToSearch) {
        return accounts.get(idToSearch) != null;
    }

    protected void transfer(String fromID, String toID, double amount) {
        double actualWithdraw = withdrawFromAcct(fromID, amount);
        depositToAcct(toID, actualWithdraw);
    }

    protected void calculateAPR(Account account) {
        aprCalculator.calculateAPR(account);
    }

    protected void chargeMinimumBalanceFee(String acctID) {
        getAcctFromBank(acctID).withdraw(25);
    }

    public String getAccountState(String acctID) {
        return accountState.getAccountStatus(getAcctFromBank(acctID));

    }
}
