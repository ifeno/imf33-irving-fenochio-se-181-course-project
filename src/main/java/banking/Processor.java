package banking;

public abstract class Processor {
    Bank bank;

    Processor(Bank bank) {
        this.bank = bank;
    }
}
