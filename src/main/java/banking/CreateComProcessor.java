package banking;

public class CreateComProcessor extends Processor {

    public CreateComProcessor(Bank bank) {
        super(bank);
    }

    public void processCommand(String[] subCommands) {
        switch (subCommands[1]) {
            case "checking":
                bank.addChecking(subCommands[2], Double.parseDouble(subCommands[3]));
                break;

            case "savings":
                bank.addSavings(subCommands[2], Double.parseDouble(subCommands[3]));
                break;

            case "cd":
                bank.addCD(subCommands[2], Double.parseDouble(subCommands[3]), Double.parseDouble(subCommands[4]));
                break;

            default:
                break;
        }
    }
}
