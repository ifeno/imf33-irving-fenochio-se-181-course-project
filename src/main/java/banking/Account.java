package banking;

import java.util.ArrayList;
import java.util.List;

public abstract class Account {
    private final String type;
    private final String id;
    private double rate;
    private double balance;

    private int numberOfWithdraws = 0;
    private double maxDeposit;
    private double maxWithdraw;
    private int acctAge = 0;
    private List<String> transactionHistory;

    Account(String type, String id) {
        this.type = type;
        this.id = id;
        this.rate = 0.0;
        this.balance = 0;
        transactionHistory = new ArrayList<>();
    }

    public String getId() {
        return this.id;
    }

    public void newRate(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return this.rate;
    }

    public double getBalance() {
        return this.balance;
    }

    public void deposit(double deposit) {
        this.balance = this.balance + deposit;
    }

    public double withdraw(double withdraw_value) {
        if ( this.balance - withdraw_value < 0) {
            double lowBalance = this.balance;
            this.balance = 0;
            return lowBalance;
        } else {
            this.balance -= withdraw_value;
            return withdraw_value;
        }
    }

    public String getAcctType() {
        return this.type;
    }

    public double getMaxDeposit() {
        return this.maxDeposit;
    }

    protected void setMaxDeposit(double maxDeposit) {
        this.maxDeposit = maxDeposit;
    }

    public double getMaxWithdraw() {
        return this.maxWithdraw;
    }

    protected void setMaxWithdraw(double maxWithdraw) {
        this.maxWithdraw = maxWithdraw;
    }

    public int getWithdrawsThisMonth() {
        return this.numberOfWithdraws;
    }

    public int getAcctAge() {
        return this.acctAge;
    }

    public void passMonth() {
        this.acctAge += 1;
        this.numberOfWithdraws = 0;
    }

    public void increaseNumWithdraws() {
        this.numberOfWithdraws += 1;
    }

    public void addTransaction(String command) {
        transactionHistory.add(command);
    }

    public List<String> getTransactions() {
        return this.transactionHistory;
    }
}