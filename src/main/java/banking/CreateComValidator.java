package banking;

import java.util.regex.Pattern;

public class CreateComValidator extends Validator {
    Pattern aprFormat = Pattern.compile("\\d{1,2}.\\d{1,2}");

    public CreateComValidator(Bank bank) {
        super(bank);
    }

    private boolean verifyAPR(String aprStr) {
            double apr = Double.parseDouble(aprStr);
            boolean rangeValid =  apr >= 0 && apr <= 10;
            boolean formatValid = aprFormat.matcher(aprStr).matches();

            return rangeValid && formatValid;
    }

    private boolean verifyCdAmount(String amountStr) {
        if (termIsNumericalToDouble(amountStr)){
            return CD.amountInCreationRange(amountStr);
        } else return false;
    }

    public boolean validate(String[] subCommands) {
        if (requiredComTerms(subCommands.length, 4, 5) &&
                subCommands[1].matches("^checking|savings|cd$") &&
                idTermValid(subCommands[2]) &&
                !idExistInBank(subCommands[2]) &&
                termIsNumericalToDouble(subCommands[3]) &&
                verifyAPR(subCommands[3])) {

            if (subCommands[1].equals("cd")) {
                return subCommands.length == 5 && verifyCdAmount(subCommands[4]);
            }
            return true;
        } else {
            return false;
        }
    }
}