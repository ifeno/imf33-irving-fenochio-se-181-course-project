package banking;

public class TransactionHistoryLogger {
    private Bank bank;
    public TransactionHistoryLogger(Bank bank) {
        this.bank = bank;
    }

    public void logToAcct(String acctID, String command) {
        Account account = bank.getAcctFromBank(acctID);
        account.addTransaction(command);
    }
}
