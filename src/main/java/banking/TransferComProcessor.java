package banking;

public class TransferComProcessor extends Processor {
    TransferComProcessor(Bank bank) {
        super(bank);
    }

    public void processCommand(String[] subCommands) {
        bank.transfer(subCommands[1], subCommands[2], Double.parseDouble(subCommands[3]));
    }
}
