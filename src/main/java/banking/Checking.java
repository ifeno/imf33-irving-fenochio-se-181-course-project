package banking;

public class Checking extends Account {

    public Checking(String id) {
        super("checking", id);
        setMaxDeposit(1000);
        setMaxWithdraw(400);
    }
}
