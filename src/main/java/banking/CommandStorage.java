package banking;

import java.util.*;

public class CommandStorage {
    Bank bank;
    private TransactionHistoryLogger transactionLogger;
    private AccountState accountState;

    List<String> invalidCommands = new ArrayList<>();

    public CommandStorage(Bank bank) {
        this.bank = bank;
        this.transactionLogger = new TransactionHistoryLogger(this.bank);
        this.accountState = new AccountState();
    }


    protected List<String> getCommandsList() {
        List<String> outputsList = new ArrayList<>();
        Set<String> accountsList = new HashSet<>(bank.getAccounts().keySet());

        for (String acctID : accountsList){
            Account account = bank.getAcctFromBank(acctID);

            outputsList.add(accountState.getAccountStatus(account));

            List<String> transactions = account.getTransactions();
            outputsList.addAll(transactions);
        }

        outputsList.addAll(getInvalidCommands());

        return outputsList;
    }

    private List<String> getInvalidCommands() {
        return this.invalidCommands;
    }

    private String[] splitCommand(String command){
        command = command.toLowerCase(Locale.ROOT);
        return command.split(" ");
    }

    protected void addInvalidCommand(String command) {
        this.invalidCommands.add(command);
    }

    protected void logTransaction(String command){
        String[] subCommands = splitCommand(command);

        switch (subCommands[0]) {
            case "deposit":

            case "withdraw":
                transactionLogger.logToAcct(subCommands[1],command);
                break;

            case "transfer":
                transactionLogger.logToAcct(subCommands[1],command);
                transactionLogger.logToAcct(subCommands[2],command);
                break;

            default:
                break;
        }

    }
}
