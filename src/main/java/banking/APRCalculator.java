package banking;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class APRCalculator {

    protected void calculateAPR(Account account) {
        int loop;
        if (account.getAcctType().equals("cd")){
            loop = 4;
        } else {
            loop = 1;
        }
        for (int i = 1 ; i <= loop ; i ++ ){
            double apr = (account.getRate() / 100) / 12;
            double interest = account.getBalance() * apr;
            account.deposit(interest);
        }
    }
}
