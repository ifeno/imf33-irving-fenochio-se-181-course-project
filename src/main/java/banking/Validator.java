package banking;

public abstract class Validator {
    public Bank bank;

    Validator(Bank bank) {
        this.bank = bank;
    }

    protected boolean requiredComTerms(int length, int minTerms, int maxTerms) {
        return length >= minTerms && length <= maxTerms;
    }

    protected boolean idExistInBank(String idTerm) {
        return bank.accountExistByQuickID(idTerm);
    }

    protected Account getAcctFromBank(String acctID) {
        return bank.getAccounts().get(acctID);
    }


    protected boolean termIsNumericalToDouble(String amountStr){
        try{
            Double.parseDouble(amountStr);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }

    protected boolean idTermValid(String idTerm) {
        if (idTerm.length() == 8) {
            try {
                Integer.parseInt(idTerm);
            } catch (NumberFormatException nfe) {
                return false;
            }
            return true;
        }
        return false;
    }

}
