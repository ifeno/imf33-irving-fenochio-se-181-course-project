package banking;

public class PassTimeComProcessor extends Processor {
    PassTime passTime;

    public PassTimeComProcessor(Bank bank) {
        super(bank);
        passTime = new PassTime(this.bank);
    }

    public void processCommand(String[] subCommands) {
        passTime.pass(Integer.parseInt(subCommands[1]));
    }
}
