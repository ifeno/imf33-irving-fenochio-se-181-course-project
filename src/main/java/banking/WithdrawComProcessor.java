package banking;

public class WithdrawComProcessor extends Processor {
    public WithdrawComProcessor(Bank bank) {
        super(bank);
    }

    public void processCommand(String[] subCommands) {
        bank.withdrawFromAcct(subCommands[1], Double.parseDouble(subCommands[2]));
    }
}
