package banking;

public class CD extends Account {
    protected final static int MIN_CREATION_AMOUNT = 1000;
    protected final static int MAX_CREATION_AMOUNT = 10000;

    public CD(String id, double apr, double amount) {
        super("cd", id);
        this.newRate(apr);
        this.deposit(amount);
        setMaxWithdraw(Double.POSITIVE_INFINITY);
    }

    public int getMinCreationAmount() {
        return MIN_CREATION_AMOUNT;
    }

    public static boolean amountInCreationRange(String amountStr){
        double amount = Double.parseDouble(amountStr);
        return amount >= CD.MIN_CREATION_AMOUNT && amount <= CD.MAX_CREATION_AMOUNT;
    }
}
