package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountTest {

    public static final String QUICK_ID = "12345678";
    Savings account;

    @BeforeEach
    void setUp() {
        account = new Savings(QUICK_ID);
    }


    @Test
    void account_has_ID() {
        assertEquals("12345678", account.getId());
    }

    @Test
    void assign_account_rate() {
        account.newRate(0.6);
        assertEquals(0.6, account.getRate());
    }

    @Test
    void account_has_initial_balance_of_zero() {
        assertEquals(0, account.getBalance());
    }

    @Test
    void depositing_into_account() {
        account.deposit(1000);
        assertEquals(1000, account.getBalance());
    }

    @Test
    void depositing_into_account_twice() {
        account.deposit(1000);
        account.deposit(500);
        assertEquals(1500, account.getBalance());
    }

    @Test
    void withdraw_from_savings() {
        account.deposit(1000);
        account.withdraw(250);
        assertEquals(750, account.getBalance());
    }

    @Test
    void withdraw_from_savings_twice() {
        account.deposit(1000);
        account.withdraw(250);
        account.withdraw(500);
        assertEquals(250, account.getBalance());
    }

    @Test
    void savings_balance_cannot_go_below_zero() {
        account.withdraw(1000);
        assertEquals(0, account.getBalance());
    }

    @Test
    void retrieve_account_type() {
        assertEquals("savings", account.getAcctType());
    }

    @Test
    void newly_created_account_has_age_0() {
        int age = account.getAcctAge();
        assertEquals(0, age);
    }

    @Test
    void increase_acctAge_by_1() {
        account.passMonth();
        int age = account.getAcctAge();
        assertEquals(1, age);
    }

    @Test
    void increase_acctAge_by_6() {
        for (int i = 1; i <= 6; i++) {
            account.passMonth();
        }
        int age = account.getAcctAge();
        assertEquals(6, age);
    }

    @Test
    void increase_acctAge_by_12() {
        for (int i = 1; i <= 12; i++) {
            account.passMonth();
        }
        int age = account.getAcctAge();
        assertEquals(12, age);
    }


}
