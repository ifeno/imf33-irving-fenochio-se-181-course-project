package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class PassTimeComProcessorTest extends CommandTestingPackage{
    PassTimeComProcessor passTimeComProcessor;

    private void assertPassTime(double expectedAmount, int expectedAge, String acctID) {
        Account account = bank.getAcctFromBank(acctID);
        assertEquals(expectedAge, account.getAcctAge());
        assertEquals(expectedAmount, round2dec(account.getBalance()) );
    }

    @BeforeEach
    void setUp() {
        bank = new Bank();
        passTimeComProcessor = new PassTimeComProcessor(bank);
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 400);

    }

    @Test
    void passtime_for_checking_age_and_balance_increase() {
        subCommands = splitCommandIntoTerms("pass 1");
        passTimeComProcessor.processCommand(subCommands);
        assertPassTime(400.2, 1, EXIST_ID);

    }

    @Test
    void passtime_for_savings_age_and_balance_increase() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        subCommands = splitCommandIntoTerms("pass 6");

        passTimeComProcessor.processCommand(subCommands);
        assertPassTime(1003.00, 6, QUICK_ID);

    }

    @Test
    void passtime_for_cd_age_and_balance_increase() {
        bank.addCD(QUICK_ID, RATE, 1000);
        subCommands = splitCommandIntoTerms("pass 12");

        passTimeComProcessor.processCommand(subCommands);
        assertPassTime(1024.28, 12, QUICK_ID);
    }

    @Test
    void passtime_closes_zero_balance_accounts() {
        bank.addSavings(QUICK_ID, RATE);
        subCommands = splitCommandIntoTerms("pass 1");

        passTimeComProcessor.processCommand(subCommands);
        assertFalse(bank.accountExistByQuickID(QUICK_ID));
    }

    @Test
    void passtime_charges_minimum_balance_fee() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 50);
        subCommands = splitCommandIntoTerms("pass 1");

        passTimeComProcessor.processCommand(subCommands);
        assertPassTime(25.01, 1, QUICK_ID);
    }

    @Test
    void passtime_for_apr_example() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 5000);
        subCommands = splitCommandIntoTerms("pass 1");

        passTimeComProcessor.processCommand(subCommands);
        assertPassTime(5002.50, 1, QUICK_ID);

    }



}
