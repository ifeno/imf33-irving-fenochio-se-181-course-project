package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DepositComValidatorTest extends CommandTestingPackage {
    public static final String Q_Command = "deposit 00000000 2500";
    protected DepositComValidator depositComValidator;


    @BeforeEach
    void setUp() {
        subCommands = Q_Command.split(" ");
        bank = new Bank();
        bank.addSavings(EXIST_ID, RATE);
        depositComValidator = new DepositComValidator(bank);
    }

    @Test
    void valid_deposit() {
        boolean actual = depositComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void id_to_deposit_to_does_not_exist() {
        command = String.format("deposit %s %d", QUICK_ID, 2000);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void missing_id() {
        command = String.format("deposit %d", 2000);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void missing_amount() {
        command = String.format("deposit %s", EXIST_ID);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void amount_too_high_savings() {
        command = String.format("deposit %s %d", EXIST_ID, 3000);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void amount_too_high_checking() {
        bank.addChecking(QUICK_ID, RATE);
        command = String.format("deposit %s %d", QUICK_ID, 2000);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void amount_too_high_cd() {
        bank.addCD(QUICK_ID, RATE, 1000);
        command = String.format("deposit %s %d", QUICK_ID, 100);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void amount_is_not_numerical_is_invalid() {
        command = String.format("deposit %s two-thousand", EXIST_ID);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void amount_can_be_zero() {
        command = String.format("deposit %s %d", EXIST_ID, 0);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void negative_amount_is_invalid() {
        command = String.format("deposit %s %d", EXIST_ID, -100);
        subCommands = command.split(" ");
        boolean actual = depositComValidator.validate(subCommands);
        assertFalse(actual);
    }


}
