package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountStateTest extends CommandTestingPackage{

    @BeforeEach
    void setUp() {
        bank = new Bank();
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID,300);

    }

    @Test
    void returned_state_of_checking_account(){
        String statement = bank.getAccountState(EXIST_ID);

        assertEquals("Checking 00000000 300.00 0.60", statement);
    }

    @Test
    void returned_state_of_saving_account_after_time_pass(){
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 400);
        Account account = bank.getAcctFromBank(QUICK_ID);
        bank.calculateAPR(account);

        String statement = bank.getAccountState(QUICK_ID);

        assertEquals("Savings 12345678 400.20 0.60", statement);
    }

}
