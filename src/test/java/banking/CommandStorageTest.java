package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandStorageTest {

    String command;
    CommandStorage commandStorage;

    @BeforeEach
    void setUp() {
        Bank bank = new Bank();
        commandStorage = new CommandStorage(bank);
    }


    @Test
    void command_storage_is_initially_empty() {
        assertTrue(commandStorage.getCommandsList().isEmpty());
    }

    @Test
    void store_one_command_string_in_storage() {
        commandStorage.addInvalidCommand("create 12345678");


        List<String> expected = new ArrayList<>();
        expected.add("create 12345678");
        assertEquals(expected, commandStorage.getCommandsList());
    }

    @Test
    void store_multiple_command_string_in_storage() {
        commandStorage.addInvalidCommand("create 12345678");
        commandStorage.addInvalidCommand("deposit 87654321");
        commandStorage.addInvalidCommand("create 11223344");


        List<String> expected = new ArrayList<>();
        expected.add("create 12345678");
        expected.add("deposit 87654321");
        expected.add("create 11223344");

        assertEquals(expected, commandStorage.getCommandsList());
    }
}
