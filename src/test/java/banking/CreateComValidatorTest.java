package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateComValidatorTest extends CommandTestingPackage {
    private static final String Q_Command = "create checking 12345678 0.6";
    protected CreateComValidator createComValidator;

    @BeforeEach
    void setUp() {
        subCommands = Q_Command.split(" ");
        bank = new Bank();
        bank.addSavings(EXIST_ID, RATE);
        createComValidator = new CreateComValidator(bank);
    }

    @Test
    void valid_create_command() {
        boolean actual = createComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void create_validator_accepts_cd_min_amount() {
        command = "create cd 12345678 0.6 1000";
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void create_validator_accepts_cd_max_amount() {
        command = "create cd 12345678 0.6 1000";
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void duplicate_quick_ID_is_invalid() {
        command = String.format("create savings %s 0.6", EXIST_ID);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void create_validator_does_not_accept_other_acct_types() {
        command = "create rothira 12345678 0.6";
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void missing_account_type() {
        command = String.format("create %s %.2f", QUICK_ID, RATE);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void missing_id() {
        command = String.format("create savings %.2f", RATE);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void short_id_is_invalid() {
        command = "create savings 1234 0.6";
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void decimal_id_is_invalid() {
        command = "create savings 12340000.00 0.6";
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void characters_in_id_is_invalid() {
        command = "create savings abcdefgh 0.6";
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void missing_rate() {
        command = String.format("create savings %s", QUICK_ID);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void rate_given_exceeds_accepted_value() {
        command = String.format("create savings %s 11", QUICK_ID);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void rate_accepts_0_and_10() {
        command = String.format("create savings %s 0.0", QUICK_ID);
        subCommands = command.split(" ");
        boolean a1 = createComValidator.validate(subCommands);
        command = String.format("create savings %s 10.00", QUICK_ID);
        subCommands = command.split(" ");
        boolean a2 = createComValidator.validate(subCommands);
        assertTrue(a1 && a2);
    }

    @Test
    void rate_given_is_not_numerical_value_is_invalid() {
        command = String.format("create savings %s one", QUICK_ID);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void negative_rate_is_invalid() {
        command = String.format("create savings %s -1.0", QUICK_ID);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void command_has_extra_terms() {
        command = String.format("create a savings %s %.2f", QUICK_ID, RATE);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void command_has_insufficient_terms() {
        command = "create";
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void create_cd_is_missing_amount() {
        command = String.format("create cd %s %.2f", QUICK_ID, RATE);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void create_cd_has_invalid_amount() {
        command = String.format("create cd %s %.2f %d", QUICK_ID, RATE, 200);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void create_cd_has_amount_larger_than_limit() {
        command = String.format("create cd %s %.2f %d", QUICK_ID, RATE, 11000);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void create_cd_amount_not_number_is_invalid() {
        command = String.format("create cd %s %.2f twenty", QUICK_ID, RATE);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void create_cd_has_negative_invalid_amount() {
        command = String.format("create cd %s %.2f %d", QUICK_ID, RATE, -200);
        subCommands = command.split(" ");
        boolean actual = createComValidator.validate(subCommands);
        assertFalse(actual);
    }


}
