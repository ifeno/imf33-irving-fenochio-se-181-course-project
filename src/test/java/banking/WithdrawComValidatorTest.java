package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WithdrawComValidatorTest extends CommandTestingPackage {
    public static final String Q_Command = "withdraw 00000000 300";
    protected WithdrawComValidator withdrawComValidator;

    @BeforeEach
    void setUp() {
        subCommands = Q_Command.split(" ");
        bank = new Bank();
        withdrawComValidator = new WithdrawComValidator(bank);
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 1000);
    }

    @Test
    void valid_withdraw() {
        actual = withdrawComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void valid_withdraw_savings() {
        bank.addSavings(QUICK_ID, RATE);
        subCommands = splitCommandIntoTerms("withdraw 12345678 500");

        actual = withdrawComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void amount_exceeds_limit_for_checking_is_invalid() {
        subCommands = splitCommandIntoTerms("withdraw 12345678 450");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void amount_exceeds_limit_for_savings_is_invalid() {
        bank.addSavings(QUICK_ID, RATE);
        subCommands = splitCommandIntoTerms("withdraw 12345678 1100");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void amount_is_non_numerical_is_invalid() {
        bank.addSavings(QUICK_ID, RATE);
        subCommands = splitCommandIntoTerms("withdraw 12345678 one-thousand");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_from_savings_twice_is_invalid() {
        bank.addSavings(QUICK_ID, RATE);
        bank.withdrawFromAcct(QUICK_ID, 1000);

        subCommands = splitCommandIntoTerms("withdraw 12345678 1000");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_from_young_cd_acct_is_invalid() {
        bank.addCD(QUICK_ID, RATE, 1000);
        subCommands = splitCommandIntoTerms("withdraw 12345678 1000");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_from_12_month_cd_is_valid() {
        bank.addCD(QUICK_ID, RATE, 1000);
        for (int i = 1; i <= 12; i++) {
            bank.getAccounts().get(QUICK_ID).passMonth();
        }

        subCommands = splitCommandIntoTerms("withdraw 12345678 1000");
        actual = withdrawComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void not_withdrawing_entire_balance_from_12_month_cd_is_invalid() {
        bank.addCD(QUICK_ID, RATE, 1000);
        for (int i = 1; i <= 12; i++) {
            bank.getAccounts().get(QUICK_ID).passMonth();
        }

        subCommands = splitCommandIntoTerms("withdraw 12345678 900");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_larger_than_balance_from_12_month_cd_is_valid() {
        bank.addCD(QUICK_ID, RATE, 1000);
        for (int i = 1; i <= 12; i++) {
            bank.getAccounts().get(QUICK_ID).passMonth();
        }

        subCommands = splitCommandIntoTerms("withdraw 12345678 1200");
        actual = withdrawComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void withdraw_from_non_existent_account_is_invalid() {
        subCommands = splitCommandIntoTerms("withdraw 12341234 450");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_amount_zero_is_valid() {
        subCommands = splitCommandIntoTerms("withdraw 00000000 0");
        actual = withdrawComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void withdraw_amount_not_numerical_is_invalid() {
        subCommands = splitCommandIntoTerms("withdraw 00000000 one-hundred");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void negative_withdraw_amount_is_invalid() {
        subCommands = splitCommandIntoTerms("withdraw 00000000 -100");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_is_missing_id_term_is_invalid() {
        subCommands = splitCommandIntoTerms("withdraw 1000 ");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_is_missing_amount_term_is_invalid() {
        subCommands = splitCommandIntoTerms("withdraw 12345678 ");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void withdraw_is_missing_terms_is_invalid() {
        subCommands = splitCommandIntoTerms("withdraw ");
        actual = withdrawComValidator.validate(subCommands);
        assertFalse(actual);
    }


}
