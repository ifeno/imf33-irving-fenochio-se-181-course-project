package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateComProcessorTest {
    private static final String QUICK_ID = "12345678";
    private static final String EXIST_ID = "00000000";
    private static final double RATE = 0.6;
    CreateComProcessor createComProcessor;
    Bank bank;
    private String command;
    private String[] subCommands;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        createComProcessor = new CreateComProcessor(bank);
        bank.addSavings(EXIST_ID, RATE);
    }

    @Test
    void process_valid_create_checking() {
        command = String.format("create checking %s %f", QUICK_ID, RATE);
        subCommands = command.split(" ");
        createComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(QUICK_ID);
        String actual = String.format("%s %s %.2f", account.getAcctType(), account.getId(), account.getRate());
        assertEquals("checking 12345678 0.60", actual);
    }

    @Test
    void process_valid_create_savings() {
        command = String.format("create savings %s %f", QUICK_ID, RATE);
        subCommands = command.split(" ");
        createComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(QUICK_ID);
        String actual = String.format("%s %s %.2f", account.getAcctType(), account.getId(), account.getRate());
        assertEquals("savings 12345678 0.60", actual);
    }

    @Test
    void process_valid_create_cd() {
        command = String.format("create cd %s %f %d", QUICK_ID, RATE, 1000);
        subCommands = command.split(" ");
        createComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(QUICK_ID);
        String actual = String.format("%s %s %.2f %.2f", account.getAcctType(), account.getId(), account.getRate(), account.getBalance());
        assertEquals("cd 12345678 0.60 1000.00", actual);
    }

    @Test
    void new_account_has_no_balance_initially() {
        command = String.format("create checking %s %f", QUICK_ID, RATE);
        subCommands = command.split(" ");
        createComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(QUICK_ID);
        assertEquals(0.00, account.getBalance());
    }
}

