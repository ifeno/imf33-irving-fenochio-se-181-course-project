package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MasterControlTest {
    MasterControl masterControl;
    List<String> input;


    private void assertingSingleCommand(String command, List<String> actual) {
        assertEquals(1, actual.size());
        assertEquals(command, actual.get(0));
    }

    @BeforeEach
    void setUp() {
        input = new ArrayList<>();
        Bank bank = new Bank();
        masterControl = new MasterControl(bank, new CommandStorage(bank), new CommandValidator(bank), new CommandProcessor(bank));
    }



    @Test
    void sample_make_sure_this_passes_unchanged_or_you_will_fail() {
        input.add("Create savings 12345678 0.6");
        input.add("Deposit 12345678 700");
        input.add("Deposit 12345678 5000");
        input.add("creAte cHecKing 98765432 0.01");
        input.add("Deposit 98765432 300");
        input.add("Transfer 98765432 12345678 300");
        input.add("Pass 1");
        input.add("Create cd 23456789 1.2 2000");
        List<String> actual = masterControl.start(input);

        assertEquals(5, actual.size());
        assertEquals("Savings 12345678 1000.50 0.60", actual.get(0));
        assertEquals("Deposit 12345678 700", actual.get(1));
        assertEquals("Transfer 98765432 12345678 300", actual.get(2));
        assertEquals("Cd 23456789 2000.00 1.20", actual.get(3));
        assertEquals("Deposit 12345678 5000", actual.get(4));
    }

    @Test
    void create_term_typo_command_is_invalid() {
        input.add("reate checking 12345678 1.5");
        List<String> actual = masterControl.start(input);

        assertingSingleCommand("reate checking 12345678 1.5", actual);
    }

    @Test
    void create_command_missing_terms_is_invalid() {
        input.add("createsavings 12345678 1.55");
        input.add("create 12345678");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("createsavings 12345678 1.55", actual.get(0));
        assertEquals("create 12345678", actual.get(1));
    }

    @Test
    void create_havings_extra_terms_is_invalid() {
        input.add("create a checking 12345678 1.5");
        List<String> actual = masterControl.start(input);

        assertingSingleCommand("create a checking 12345678 1.5", actual);
    }

    @Test
    void creating_cd_is_not_invalid() {
        input.add("create cd 12345678 1.5 1000");

        List<String> actual = masterControl.start(input);

        assertingSingleCommand("Cd 12345678 1000.00 1.50", actual);
    }

    @Test
    void creating_cd_with_less_than_1000_is_invalid() {
        input.add("create cd 12345678 1.5 500");
        List<String> actual = masterControl.start(input);

        assertingSingleCommand("create cd 12345678 1.5 500", actual);
    }

    @Test
    void creating_account_with_negative_apr_is_invalid() {
        input.add("create checking 12345678 -1.5");
        List<String> actual = masterControl.start(input);

        assertingSingleCommand("create checking 12345678 -1.5", actual);
    }

    @Test
    void deposit_term_typo_command_is_invalid() {
        input.add("deosit 12345678 100");
        List<String> actual = masterControl.start(input);

        assertingSingleCommand("deosit 12345678 100", actual);
    }

    @Test
    void deposit_to_nonexistent_account_is_invalid() {
        input.add("deposit 12345678 100");
        List<String> actual = masterControl.start(input);

        assertingSingleCommand("deposit 12345678 100", actual);
    }

    @Test
    void leading_white_space_invalid(){
        input.add(" create checking 12345678 0.6");

        List<String> actual = masterControl.start(input);

        assertingSingleCommand(" create checking 12345678 0.6", actual);
    }

    @Test
    void middle_white_space_invalid(){
        input.add("create checking  12345678 0.6");

        List<String> actual = masterControl.start(input);

        assertingSingleCommand("create checking  12345678 0.6", actual);
    }

    @Test
    void trailing_white_space_is_valid(){
        input.add("create checking 12345678 0.6      ");

        List<String> actual = masterControl.start(input);

        assertingSingleCommand("Checking 12345678 0.00 0.60", actual);
    }

    @Test
    void deposit_command_missing_terms_is_invalid() {
        input.add("deposit12345678 155");
        input.add("deposit");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("deposit12345678 155", actual.get(0));
        assertEquals("deposit", actual.get(1));
    }

    @Test
    void two_commands_are_invalid() {
        input.add("reate checking 12345678 1.5");
        input.add("deosit 12345678 100");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("reate checking 12345678 1.5", actual.get(0));
        assertEquals("deosit 12345678 100", actual.get(1));
    }

    @Test
    void creating_account_with_same_id_is_invalid() {
        input.add("create checking 12345678 1.5");
        input.add("deposit 12345678 400");
        input.add("create checking 12345678 1.5");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 12345678 400.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 400", actual.get(1));
        assertEquals("create checking 12345678 1.5", actual.get(2));
    }

    @Test
    void varying_capitalization_is_not_invalid() {
        input.add("CrEaTe cHeCkiNg 12345678 1.5");

        List<String> actual = masterControl.start(input);


        assertEquals(1, actual.size());
        assertEquals("Checking 12345678 0.00 1.50", actual.get(0));
    }

    @Test
    void deposit_amount_exceeds_checking_max() {
        input.add("create checking 12345678 1.5");
        input.add("deposit 12345678 2000");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Checking 12345678 0.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 2000", actual.get(1));
    }

    @Test
    void deposit_amount_exceeds_saving_max() {
        input.add("create savings 12345678 1.5");
        input.add("deposit 12345678 3000");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Savings 12345678 0.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 3000", actual.get(1));
    }

    @Test
    void deposit_amount_exceeds_cd_max() {
        input.add("create cd 12345678 1.5 1000");
        input.add("deposit 12345678 1");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Cd 12345678 1000.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 1", actual.get(1));


    }

    @Test
    void valid_withdraw_from_checking() {
        input.add("create checking 12345678 1.5");
        input.add("deposit 12345678 400");
        input.add("withdraw 12345678 200");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 12345678 200.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 400", actual.get(1));
        assertEquals("withdraw 12345678 200", actual.get(2));
    }

    @Test
    void capitalization_valid_withdraw_from_checking() {
        input.add("CrEaTe cHeCkiNg 12345678 1.5");
        input.add("dePosiT 12345678 400");
        input.add("wIthDrAw 12345678 200");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 12345678 200.00 1.50", actual.get(0));
        assertEquals("dePosiT 12345678 400", actual.get(1));
        assertEquals("wIthDrAw 12345678 200",actual.get(2));
    }

    @Test
    void withdraw_amount_exceeds_allowed_checking_amount() {
        input.add("create checking 12345678 1.5");
        input.add("deposit 12345678 1000");
        input.add("withdraw 12345678 450");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 12345678 1000.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 1000", actual.get(1));
        assertEquals("withdraw 12345678 450",actual.get(2));
    }

    @Test
    void withdraw_missing_amount_is_invalid() {
        input.add("create checking 12345678 1.5");
        input.add("deposit 12345678 400");
        input.add("withdraw 12345678");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 12345678 400.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 400", actual.get(1));
        assertEquals("withdraw 12345678",actual.get(2));
    }

    @Test
    void withdraw_missing_id_is_invalid() {
        input.add("create checking 12345678 1.5");
        input.add("deposit 12345678 400");
        input.add("withdraw 200");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 12345678 400.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 400", actual.get(1));
        assertEquals("withdraw 200",actual.get(2));

    }

    @Test
    void valid_withdraw_from_saving() {
        input.add("create savings 12345678 1.5");
        input.add("deposit 12345678 2500");
        input.add("withdraw 12345678 1000");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Savings 12345678 1500.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 2500", actual.get(1));
        assertEquals("withdraw 12345678 1000",actual.get(2));
        }

    @Test
    void withdraw_from_saving_twice_invalid() {
        input.add("create savings 12345678 1.5");
        input.add("deposit 12345678 2500");
        input.add("withdraw 12345678 1000");
        input.add("withdraw 12345678 500");

        List<String> actual = masterControl.start(input);

        assertEquals(4, actual.size());
        assertEquals("Savings 12345678 1500.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 2500", actual.get(1));
        assertEquals("withdraw 12345678 1000",actual.get(2));
        assertEquals("withdraw 12345678 500",actual.get(3));
    }

    @Test
    void second_withdraw_from_savings_valid_after_timepass() {
        input.add("create savings 12345678 1.5");
        input.add("deposit 12345678 2500");
        input.add("withdraw 12345678 1000");
        input.add("pass 1");
        input.add("withdraw 12345678 500");

        List<String> actual = masterControl.start(input);

        assertEquals(4, actual.size());
        assertEquals("Savings 12345678 1001.87 1.50", actual.get(0));
        assertEquals("deposit 12345678 2500", actual.get(1));
        assertEquals("withdraw 12345678 1000",actual.get(2));
        assertEquals("withdraw 12345678 500",actual.get(3));
    }


    @Test
    void valid_withdraw_from_cd() {
        input.add("create cd 12345678 1.5 1000");
        input.add("pass 12");
        input.add("withdraw 12345678 1061.80");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Cd 12345678 0.00 1.50", actual.get(0));
        assertEquals("withdraw 12345678 1061.80", actual.get(1));
    }

    @Test
    void withdraw_from_young_cd_invalid() {
        input.add("create cd 12345678 1.5 1000");
        input.add("withdraw 12345678 1000");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Cd 12345678 1000.00 1.50", actual.get(0));
        assertEquals("withdraw 12345678 1000", actual.get(1));
    }

    @Test
    void withdraw_from_cd_not_for_full_amount() {
        input.add("create cd 12345678 1.5 1000");
        input.add("pass 12");
        input.add("withdraw 12345678 900");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Cd 12345678 1061.79 1.50", actual.get(0));
        assertEquals("withdraw 12345678 900", actual.get(1));
    }


    @Test
    void valid_transfer_checking_to_checking() {
        input.add("create checking 00000000 1.5");
        input.add("create checking 12345678 1.5");
        input.add("deposit 00000000 1000");
        input.add("transfer 00000000 12345678 400");

        List<String> actual = masterControl.start(input);

        assertEquals(5, actual.size());
        assertEquals("Checking 00000000 600.00 1.50", actual.get(0));
        assertEquals("deposit 00000000 1000", actual.get(1));
        assertEquals("transfer 00000000 12345678 400", actual.get(2));
        assertEquals("Checking 12345678 400.00 1.50", actual.get(3));
        assertEquals("transfer 00000000 12345678 400", actual.get(4));
    }

    @Test
    void valid_transfer_unaffected_by_capitalizaion() {
        input.add("create checking 00000000 1.5");
        input.add("create checking 12345678 1.5");
        input.add("deposit 00000000 1000");
        input.add("TrAnsFeR 00000000 12345678 400");

        List<String> actual = masterControl.start(input);

        assertEquals(5, actual.size());
        assertEquals("Checking 00000000 600.00 1.50", actual.get(0));
        assertEquals("deposit 00000000 1000", actual.get(1));
        assertEquals("TrAnsFeR 00000000 12345678 400", actual.get(2));
        assertEquals("Checking 12345678 400.00 1.50", actual.get(3));
        assertEquals("TrAnsFeR 00000000 12345678 400", actual.get(4));
    }

    @Test
    void invalid_transfer_exceeds_checking_withdraw_amount() {
        input.add("create checking 00000000 1.5");
        input.add("create checking 12345678 1.5");
        input.add("deposit 00000000 1000");
        input.add("transfer 00000000 12345678 1000");

        List<String> actual = masterControl.start(input);

        assertEquals(4, actual.size());
        assertEquals("Checking 00000000 1000.00 1.50", actual.get(0));
        assertEquals("deposit 00000000 1000", actual.get(1));
        assertEquals("Checking 12345678 0.00 1.50", actual.get(2));
        assertEquals("transfer 00000000 12345678 1000", actual.get(3));
    }

    @Test
    void invalid_transfer_from_nonexistent_account() {
        input.add("create checking 12345678 1.5");
        input.add("transfer 00000000 12345678 400");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Checking 12345678 0.00 1.50", actual.get(0));
        assertEquals("transfer 00000000 12345678 400", actual.get(1));
    }

    @Test
    void invalid_transfer_to_nonexistent_account() {
        input.add("create checking 00000000 1.5");
        input.add("deposit 00000000 1000");
        input.add("transfer 00000000 12345678 400");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 00000000 1000.00 1.50", actual.get(0));
        assertEquals("deposit 00000000 1000", actual.get(1));
        assertEquals("transfer 00000000 12345678 400", actual.get(2));
    }

    @Test
    void invalid_transfer_exceeds_savings_withdraw_amount() {
        input.add("create savings 00000000 1.5");
        input.add("create savings 12345678 1.5");
        input.add("deposit 00000000 2500");
        input.add("transfer 00000000 12345678 2500");

        List<String> actual = masterControl.start(input);

        assertEquals(4, actual.size());
        assertEquals("Savings 00000000 2500.00 1.50", actual.get(0));
        assertEquals("deposit 00000000 2500", actual.get(1));
        assertEquals("Savings 12345678 0.00 1.50", actual.get(2));
        assertEquals("transfer 00000000 12345678 2500", actual.get(3));
    }
    @Test
    void invalid_transfer_from_account_invalid_id() {
        input.add("create checking 12345678 1.5");
        input.add("transfer abcdefgh 12345678 400");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("Checking 12345678 0.00 1.50", actual.get(0));
        assertEquals("transfer abcdefgh 12345678 400", actual.get(1));
    }

    @Test
    void invalid_transfer_to_account_invalid_id() {
        input.add("create checking 00000000 1.5");
        input.add("deposit 00000000 1000");
        input.add("transfer 12345678 abcdefgh 400");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Checking 00000000 1000.00 1.50", actual.get(0));
        assertEquals("deposit 00000000 1000", actual.get(1));
        assertEquals("transfer 12345678 abcdefgh 400", actual.get(2));
    }

    @Test
    void invalid_transfer_of_nonnumerical_amount() {
        input.add("create checking 00000000 1.5");
        input.add("create checking 12345678 1.5");
        input.add("deposit 00000000 1000");
        input.add("transfer 00000000 12345678 four-hundred");

        List<String> actual = masterControl.start(input);

        assertEquals(4, actual.size());
        assertEquals("Checking 00000000 1000.00 1.50", actual.get(0));
        assertEquals("deposit 00000000 1000", actual.get(1));
        assertEquals("Checking 12345678 0.00 1.50", actual.get(2));
        assertEquals("transfer 00000000 12345678 four-hundred", actual.get(3));
    }


    @Test
    void transfer_from_young_cd_invalid() {
        input.add("create cd 00000000 1.5 1000");
        input.add("create savings 12345678 1.5");
        input.add("transfer 00000000 12345678 1000");

        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Cd 00000000 1000.00 1.50", actual.get(0));
        assertEquals("Savings 12345678 0.00 1.50", actual.get(1));
        assertEquals("transfer 00000000 12345678 1000", actual.get(2));
    }


        @Test
    void transfer_from_12_months_cd_invalid() {
        input.add("create cd 00000000 1.5 1000");
        input.add("pass 12");
        input.add("create savings 12345678 0.6");
        input.add("transfer 00000000 12345678 1061.80");


        List<String> actual = masterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Cd 00000000 1061.79 1.50", actual.get(0));
        assertEquals("Savings 12345678 0.00 0.60", actual.get(1));
        assertEquals("transfer 00000000 12345678 1061.80", actual.get(2));
    }

    @Test
    void passtime_for_invalid_time_no_numerical() {
        input.add("create savings 12345678 1.5");
        input.add("deposit 12345678 2500");
        input.add("withdraw 12345678 1000");
        input.add("pass one");
        input.add("withdraw 12345678 500");

        List<String> actual = masterControl.start(input);

        assertEquals(5, actual.size());
        assertEquals("Savings 12345678 1500.00 1.50", actual.get(0));
        assertEquals("deposit 12345678 2500", actual.get(1));
        assertEquals("withdraw 12345678 1000",actual.get(2));
        assertEquals("pass one",actual.get(3));
        assertEquals("withdraw 12345678 500",actual.get(4));
    }
}
