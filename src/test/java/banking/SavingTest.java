package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SavingTest {

    public static final String ID = "12345678";
    Savings savings;

    @BeforeEach
    void setUp() {
        savings = new Savings(ID);
        savings.newRate(0.6);
    }

    @Test
    void savings_has_ID() {
        assertEquals("12345678", savings.getId());
    }

    @Test
    void assign_savings_rate() {
        assertEquals(0.6, savings.getRate());
    }

    @Test
    void savings_has_initial_balance_of_zero() {
        assertEquals(0, savings.getBalance());
    }

    @Test
    void get_max_deposit_savings() {
        assertEquals(2500, savings.getMaxDeposit());
    }

    @Test
    void get_max_withdraw_savings() {
        assertEquals(1000, savings.getMaxWithdraw());
    }
}
