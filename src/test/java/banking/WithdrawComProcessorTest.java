package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WithdrawComProcessorTest extends CommandTestingPackage {
    WithdrawComProcessor withdrawComProcessor;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        withdrawComProcessor = new WithdrawComProcessor(bank);
    }

    @Test
    void valid_withdraw_from_checking() {
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 400);

        command = String.format("withdraw %s %d", EXIST_ID, 150);
        subCommands = command.split(" ");
        withdrawComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(250.00, account.getBalance());
    }

    @Test
    void valid_withdraw_from_savings() {
        bank.addSavings(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 400);

        command = String.format("withdraw %s %d", EXIST_ID, 150);
        subCommands = command.split(" ");
        withdrawComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(250.00, account.getBalance());
    }

    @Test
    void valid_withdraw_from_cd() {
        bank.addSavings(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 1000);

        command = String.format("withdraw %s %d", EXIST_ID, 1000);
        subCommands = command.split(" ");
        withdrawComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(0, account.getBalance());
    }


}
