package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PassTimeComValidatorTest extends CommandTestingPackage{
    PassTimeComValidator passTimeComValidator;
    public static final String Q_Command = "pass 1";

    @BeforeEach
    void setUp() {
        subCommands = Q_Command.split(" ");
        bank = new Bank();
        passTimeComValidator = new PassTimeComValidator(bank);
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 1000);
    }

    @Test
    void valid_pass_time_command(){
        actual = passTimeComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void duration_term_is_zero_is_invalid(){
        subCommands = splitCommandIntoTerms("pass 0");
        actual = passTimeComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void duration_term_is_over_60_is_invalid(){
        subCommands = splitCommandIntoTerms("pass 61");
        actual = passTimeComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void duration_term_is_not_int_is_invalid(){
        subCommands = splitCommandIntoTerms("pass 1.0");
        actual = passTimeComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void duration_term_is_not_numerical_int_is_invalid(){
        subCommands = splitCommandIntoTerms("pass one");
        actual = passTimeComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void duration_term_is_missing_is_invalid(){
        subCommands = splitCommandIntoTerms("pass");
        actual = passTimeComValidator.validate(subCommands);
        assertFalse(actual);
    }
}

