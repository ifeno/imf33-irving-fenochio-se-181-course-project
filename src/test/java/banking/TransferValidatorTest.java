package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransferValidatorTest extends CommandTestingPackage{
    public static final String Q_Command = "transfer 00000000 12345678 400";
    private WithdrawComValidator w;
    private DepositComValidator d;
    protected TransferComValidator transferComValidator;

    @BeforeEach
    void setUp() {
        subCommands = Q_Command.split(" ");
        bank = new Bank();
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 400);
        d = new DepositComValidator(bank);
        w = new WithdrawComValidator(bank);
        transferComValidator = new TransferComValidator(bank, d, w);
    }

    @Test
    void valid_transfer() {
        bank.addChecking(QUICK_ID, RATE);
        actual = transferComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void valid_transfer_from_checking_to_savings() {
        bank.addSavings(QUICK_ID, RATE);
        actual = transferComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void valid_transfer_from_savings_to_checking() {
        bank.addSavings(QUICK_ID, RATE);
        command = String.format("transfer %s %s %d",EXIST_ID, QUICK_ID, 400);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void valid_transfer_from_savings_to_savings() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        bank.addSavings("12341234", RATE);
        command = String.format("transfer %s 12341234 %d", QUICK_ID, 1000);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void transfer_to_cd_is_invalid(){
        bank.addCD(QUICK_ID, RATE, 1000);
        command = String.format("transfer %s %s %d", EXIST_ID, QUICK_ID, 400);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_from_new_cd_is_invalid(){
        bank.addCD(QUICK_ID, RATE, 1000);
        command = String.format("transfer %s %s %d", QUICK_ID, EXIST_ID, 400);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_from_12_month_cd_is_invalid(){
        bank.addCD(QUICK_ID, RATE, 1000);
        for (int i = 1; i <= 12; i++ ){
            bank.getAccounts().get(QUICK_ID).passMonth();
        }
        command = String.format("transfer %s %s %f", QUICK_ID, EXIST_ID, bank.getAcctFromBank(QUICK_ID).getBalance());
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_to_same_account_is_invalid(){
        command = String.format("transfer %s %s %d", QUICK_ID, QUICK_ID, 400);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_to_nonexistent_account_is_invalid(){
        command = String.format("transfer %s 12341234 %d", QUICK_ID, 400);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_from_nonexistent_account_is_invalid(){
        command = String.format("transfer 12341234 %s %d", QUICK_ID, 400);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_amount_not_numerical_is_invalid(){
        bank.addChecking(QUICK_ID, RATE);
        command = String.format("transfer %s %s four-hundred", EXIST_ID, QUICK_ID);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_exceeds_withdraw_amount_for_checking(){
        bank.addChecking(QUICK_ID, RATE);
        command = String.format("transfer %s %s %d",EXIST_ID, QUICK_ID, 450);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void transfer_exceeds_withdraw_amount_for_checking_and_balance_is_less(){
        bank.addChecking(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 300);
        command = String.format("transfer %s %s %d", QUICK_ID, EXIST_ID, 450);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertFalse(actual);
    }

    @Test
    void savings_withdraw_is_checking_max_deposit_is_valid(){
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        command = String.format("transfer %s %s %d", QUICK_ID, EXIST_ID, 1000);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void transfer_withdraw_amount_is_greater_than_account_balance_is_valid(){
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 500);
        command = String.format("transfer %s %s %d", QUICK_ID, EXIST_ID, 1000);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertTrue(actual);
    }

    @Test
    void transfer_low_balance_withdraw_amount_is_valid_deposit_amount(){
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 400);
        command = String.format("transfer %s %s %d", QUICK_ID, EXIST_ID, 500);
        subCommands = splitCommandIntoTerms(command);
        actual = transferComValidator.validate(subCommands);
        assertTrue(actual);
    }
}
