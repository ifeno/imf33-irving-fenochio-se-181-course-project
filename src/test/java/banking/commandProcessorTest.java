package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class commandProcessorTest {
    public static final String QUICK_ID = "12345678";
    public static final String EXIST_ID = "00000000";
    public static final double RATE = 0.6;
    CommandProcessor commandProcessor;
    String command;
    Bank bank;


    @BeforeEach
    void setUp() {
        bank = new Bank();
        commandProcessor = new CommandProcessor(bank);
        bank.addSavings(EXIST_ID, RATE);
    }

    @Test
    void create_checking_account_successfully_creates_checking() {
        command = String.format("create checking %s %f", QUICK_ID, RATE);
        commandProcessor.processCommand(command);
        Account account = bank.getAccounts().get(QUICK_ID);
        String actual = String.format("%s %s %.2f", account.getAcctType(), account.getId(), account.getRate());
        assertEquals("checking 12345678 0.60", actual);
    }

    @Test
    void create_savings_account_successfully_creates_savings() {
        command = String.format("create savings %s %f", QUICK_ID, RATE);
        commandProcessor.processCommand(command);
        Account account = bank.getAccounts().get(QUICK_ID);
        String actual = String.format("%s %s %.2f", account.getAcctType(), account.getId(), account.getRate());
        assertEquals("savings 12345678 0.60", actual);
    }

    @Test
    void create_cd_account_successfully_creates_cd() {
        command = String.format("create cd %s %f %d", QUICK_ID, RATE, 1000);
        commandProcessor.processCommand(command);
        Account account = bank.getAccounts().get(QUICK_ID);
        String actual = String.format("%s %s %.2f %.2f", account.getAcctType(), account.getId(), account.getRate(), account.getBalance());
        assertEquals("cd 12345678 0.60 1000.00", actual);
    }

    @Test
    void newly_created_account_has_no_balance_initially() {
        command = String.format("create checking %s %f", QUICK_ID, RATE);
        commandProcessor.processCommand(command);
        Account account = bank.getAccounts().get(QUICK_ID);
        assertEquals(0.00, account.getBalance());
    }

    @Test
    void deposit_into_empty_account() {
        command = String.format("deposit %s %d", EXIST_ID, 150);
        commandProcessor.processCommand(command);
        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(150.00, account.getBalance());
    }

    @Test
    void deposit_can_use_decimal_amounts_also() {
        command = String.format("deposit %s %f", EXIST_ID, 150.55);
        commandProcessor.processCommand(command);
        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(150.55, account.getBalance());
    }

    @Test
    void deposit_into_newly_created_account() {
        command = String.format("create checking %s %f", QUICK_ID, RATE);
        commandProcessor.processCommand(command);
        command = String.format("deposit %s %d", QUICK_ID, 150);
        commandProcessor.processCommand(command);
        Account account = bank.getAccounts().get(QUICK_ID);
        assertEquals(150.00, account.getBalance());
    }

    @Test
    void deposit_into_existing_account_multiple_times() {
        command = String.format("deposit %s %f", EXIST_ID, 225.25);
        commandProcessor.processCommand(command);

        command = String.format("deposit %s %d", EXIST_ID, 150);
        commandProcessor.processCommand(command);

        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(375.25, account.getBalance());
    }

}
