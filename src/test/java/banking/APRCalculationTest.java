package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.lang.Math.round;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class APRCalculationTest extends CommandTestingPackage{


    @BeforeEach
    void setUp(){
        bank = new Bank();
        bank.addChecking(EXIST_ID, 1.5);
        bank.depositToAcct(EXIST_ID,1000);

    }

    @Test
    void calculate_checking_apr(){
        Account account = bank.getAcctFromBank(EXIST_ID);
        bank.calculateAPR(account);
        double newBalance = bank.getAcctFromBank(EXIST_ID).getBalance();
        assertEquals(1001.25, newBalance);
    }

    @Test
    void calculate_savings_apr(){
        bank.addSavings(QUICK_ID, 2.5);
        bank.depositToAcct(QUICK_ID, 1000);
        Account account = bank.getAcctFromBank(QUICK_ID);
        bank.calculateAPR(account);
        double newBalance = bank.getAcctFromBank(QUICK_ID).getBalance();
        assertEquals(1002.08, round2dec(newBalance));
    }

    @Test
    void calculate_cd_apr(){
        bank.addCD(QUICK_ID, 3.5, 1000);
        Account account = bank.getAcctFromBank(QUICK_ID);
        bank.calculateAPR(account);
        double newBalance = bank.getAcctFromBank(QUICK_ID).getBalance();
        assertEquals(1011.72, round2dec(newBalance));
    }
}
