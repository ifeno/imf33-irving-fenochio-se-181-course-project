package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandValidatorTest {
    public static final String QUICK_ID = "12345678";
    public static final String EXIST_ID = "00000000";
    public static final double RATE = 0.6;
    public static final String Q_Command = String.format("create savings %s %.2f ", QUICK_ID, RATE);
    CommandValidator commandValidator;
    Bank bank;
    String command;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        commandValidator = new CommandValidator(bank);
        bank.addSavings(EXIST_ID, RATE);
    }

    @Test
    void valid_command() {
        boolean actual = commandValidator.validate(Q_Command);
        assertTrue(actual);
    }

    @Test
    void capitalization_does_not_affect_command() {
        command = String.format("cReAtE cHeCkInG %s %.2f", QUICK_ID, RATE);
        boolean actual = commandValidator.validate(command);
        assertTrue(actual);
    }

    @Test
    void create_in_create_command_missing_invalid() {
        command = String.format("checking %s %.2f", QUICK_ID, RATE);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void create_command_has_typo() {
        command = String.format("creat savings %s %.2f", QUICK_ID, RATE);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void create_command_is_missing_at_least_one_term() {
        command = String.format("createsavings %s %.2f", QUICK_ID, RATE);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void create_command_is_missing_two_terms() {
        command = String.format("create %s", QUICK_ID);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void other_account_types_not_accepted_by_create_command() {
        command = String.format("create ira %s %.2f", QUICK_ID, RATE);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void valid_deposit() {
        command = String.format("deposit %s %d", EXIST_ID, 2000);
        boolean actual = commandValidator.validate(command);
        assertTrue(actual);
    }

    @Test
    void id_to_deposit_to_does_not_exist() {
        command = String.format("deposit %s %d", QUICK_ID, 2000);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void deposit_missing_deposit_term() {
        command = String.format("%s %d", EXIST_ID, 2000);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void deposit_missing_id() {
        command = String.format("deposit %d", 2000);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }


    @Test
    void deposit_missing_amount() {
        command = String.format("deposit %s", EXIST_ID);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }


    @Test
    void typo_in_deposit_term() {
        command = String.format("eposit %s %d", EXIST_ID, 2000);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void deposit_amount_too_high_savings() {
        command = String.format("deposit %s %d", EXIST_ID, 3000);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void deposit_amount_too_high_checking() {
        bank.addChecking(QUICK_ID, RATE);
        command = String.format("deposit %s %d", QUICK_ID, 2000);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

    @Test
    void deposit_amount_too_high_cd() {
        bank.addCD(QUICK_ID, RATE, 1000);
        command = String.format("deposit %s %d", QUICK_ID, 100);
        boolean actual = commandValidator.validate(command);
        assertFalse(actual);
    }

}



