package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BankTest {
    public static final String QUICK_ID = "12345678";
    public static final double RATE = 0.6;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
    }

    @Test
    void bank_has_no_acct_initially() {
        assertTrue(bank.getAccounts().isEmpty());
    }

    @Test
    void add_account_to_bank() {
        bank.addSavings(QUICK_ID, RATE);
        assertEquals(QUICK_ID, bank.getAccounts().get(QUICK_ID).getId());
    }

    @Test
    void add_two_accounts_to_bank() {
        bank.addSavings(QUICK_ID, RATE);
        bank.addChecking(QUICK_ID + "1", RATE + 0.5);
        assertEquals(QUICK_ID + "1", bank.getAccounts().get(QUICK_ID + "1").getId());
    }

    @Test
    void add_all_account_types_to_bank() {
        bank.addSavings(QUICK_ID, RATE);
        bank.addChecking(QUICK_ID + "1", RATE + 0.5);
        bank.addCD(QUICK_ID + "2", RATE + 0.3, 1000);
        assertEquals(QUICK_ID, bank.getAccounts().get(QUICK_ID).getId());
        assertEquals(QUICK_ID + "1", bank.getAccounts().get(QUICK_ID + "1").getId());
        assertEquals(QUICK_ID + "2", bank.getAccounts().get(QUICK_ID + "2").getId());
    }

    @Test
    void deposit_to_account_from_bank() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        assertEquals(1000, bank.getAccounts().get(QUICK_ID).getBalance());
    }

    @Test
    void deposit_to_account_from_bank_twice() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        bank.depositToAcct(QUICK_ID, 2000);
        assertEquals(3000, bank.getAccounts().get(QUICK_ID).getBalance());
    }

    @Test
    void withdraw_from_account_from_bank() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        bank.withdrawFromAcct(QUICK_ID, 250);
        assertEquals(750, bank.getAccounts().get(QUICK_ID).getBalance());
    }

    @Test
    void withdraw_from_account_from_bank_twice() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        bank.withdrawFromAcct(QUICK_ID, 250);
        bank.withdrawFromAcct(QUICK_ID, 500);
        assertEquals(250, bank.getAccounts().get(QUICK_ID).getBalance());
    }

    @Test
    void retrieve_account_type_from_bank() {
        bank.addSavings(QUICK_ID, RATE);
        assertEquals("savings", bank.getAccounts().get(QUICK_ID).getAcctType());
    }

    @Test
    void transfer_from_checking_to_checking(){
        bank.addChecking(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 200);
        bank.addChecking("00000000", RATE);
        bank.transfer(QUICK_ID, "00000000", 200);

        Account account = bank.getAccounts().get("00000000");
        assertEquals(200, account.getBalance());
    }

    @Test
    void transfer_from_checking_to_checking_had_insufficient_bal(){
        bank.addChecking(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 150);
        bank.addChecking("00000000", RATE);
        bank.transfer(QUICK_ID, "00000000", 200);

        Account account = bank.getAccounts().get("00000000");
        assertEquals(150, account.getBalance());
    }
}
