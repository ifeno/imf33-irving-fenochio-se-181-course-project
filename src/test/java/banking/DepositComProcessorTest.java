package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepositComProcessorTest extends CommandTestingPackage {
    DepositComProcessor depositComProcessor;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        depositComProcessor = new DepositComProcessor(bank);
        bank.addSavings(EXIST_ID, RATE);
    }

    @Test
    void deposit_into_empty_account() {
        command = String.format("deposit %s %d", EXIST_ID, 150);
        subCommands = command.split(" ");
        depositComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(150.00, account.getBalance());
    }

    @Test
    void deposit_accepts_decimal_values() {
        command = String.format("deposit %s %f", EXIST_ID, 150.55);
        subCommands = command.split(" ");
        depositComProcessor.processCommand(subCommands);
        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(150.55, account.getBalance());
    }

    @Test
    void deposit_into_existing_account_multiple_times() {
        command = String.format("deposit %s %f", EXIST_ID, 225.25);
        subCommands = command.split(" ");
        depositComProcessor.processCommand(subCommands);

        command = String.format("deposit %s %d", EXIST_ID, 150);
        subCommands = command.split(" ");
        depositComProcessor.processCommand(subCommands);

        Account account = bank.getAccounts().get(EXIST_ID);
        assertEquals(375.25, account.getBalance());
    }
}
