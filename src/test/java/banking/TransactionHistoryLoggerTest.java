package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionHistoryLoggerTest extends CommandTestingPackage {
    private DepositComProcessor depositProcessor;
    private WithdrawComProcessor withdrawProcessor;
    private TransferComProcessor transferProcessor;
    private TransactionHistoryLogger transactionLogger;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        this.transactionLogger = new TransactionHistoryLogger(this.bank);
        this.depositProcessor = new DepositComProcessor(this.bank);
        this.withdrawProcessor = new WithdrawComProcessor(this.bank);
        this.transferProcessor = new TransferComProcessor(this.bank);

        bank.addSavings(EXIST_ID, RATE);
    }

    @Test
    void adds_to_account_transaction_history(){
        command = "deposit 00000000 1000";
        transactionLogger.logToAcct("00000000", command);
        List<String> acutal = bank.getAcctFromBank(EXIST_ID).getTransactions();

        assertEquals("deposit 00000000 1000", acutal.get(0) );
    }

    @Test
    void adds_two_transactions_to_account_transaction_history(){
        command = "deposit 00000000 1000";
        transactionLogger.logToAcct("00000000", command);
        command = "withdraw 00000000 500";
        transactionLogger.logToAcct(EXIST_ID, command);
        List<String> acutal = bank.getAcctFromBank(EXIST_ID).getTransactions();

        assertEquals("deposit 00000000 1000", acutal.get(0) );
        assertEquals("withdraw 00000000 500", acutal.get(1) );
    }

    @Test
    void logs_to_two_different_accounts(){
        bank.addSavings(QUICK_ID, RATE);
        command = String.format("transfer %s %s 1000", QUICK_ID, EXIST_ID);
        transactionLogger.logToAcct(EXIST_ID, command);
        transactionLogger.logToAcct(QUICK_ID, command);

        List<String> log1 = bank.getAcctFromBank(EXIST_ID).getTransactions();
        List<String> log2 = bank.getAcctFromBank(QUICK_ID).getTransactions();

        assertEquals("transfer 12345678 00000000 1000", log1.get(0) );
        assertEquals("transfer 12345678 00000000 1000", log2.get(0) );
    }
}

