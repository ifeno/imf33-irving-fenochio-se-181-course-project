package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransferComProcessorTest extends CommandTestingPackage {
    TransferComProcessor transferComProcessor;

    private void assertTwoAcctBalances(String fromID, double expectedFromBal, String toID, double expectedToBalance ){
        Account acctFrom = bank.getAccounts().get(fromID);
        Account acctTo = bank.getAccounts().get(toID);
        assertEquals(expectedFromBal, acctFrom.getBalance());
        assertEquals(expectedToBalance, acctTo.getBalance());
    }

    @BeforeEach
    void setUp() {
        bank = new Bank();
        transferComProcessor = new TransferComProcessor(bank);
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 400);
    }

    @Test
    void valid_transfer_checking_to_checking() {
        bank.addChecking(QUICK_ID, RATE);

        command = String.format("transfer %s %s %d", EXIST_ID, QUICK_ID, 400);
        subCommands = command.split(" ");
        transferComProcessor.processCommand(subCommands);
        assertTwoAcctBalances(EXIST_ID, 0, QUICK_ID, 400.00);
    }

    @Test
    void valid_transfer_checking_to_savings() {
        bank.addSavings(QUICK_ID, RATE);

        command = String.format("transfer %s %s %f", EXIST_ID, QUICK_ID, 400.00);
        subCommands = command.split(" ");
        transferComProcessor.processCommand(subCommands);
        assertTwoAcctBalances(EXIST_ID, 0, QUICK_ID, 400.00);
    }

    @Test
    void valid_transfer_savings_to_checking() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 250);

        command = String.format("transfer %s %s %f", QUICK_ID, EXIST_ID, 250.00);
        subCommands = command.split(" ");
        transferComProcessor.processCommand(subCommands);

        assertTwoAcctBalances(QUICK_ID, 0, EXIST_ID, 650.00);
    }

    @Test
    void valid_transfer_savings_to_savings() {
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 250);

        bank.addSavings("12341234", RATE);
        bank.depositToAcct("12341234", 250);

        command = String.format("transfer %s %s %f", QUICK_ID, "12341234", 250.00);
        subCommands = command.split(" ");
        transferComProcessor.processCommand(subCommands);

        assertTwoAcctBalances(QUICK_ID, 0, "12341234", 500.00);
    }

    @Test
    void valid_transfer_from_12_month_cd() {
        bank.addCD(QUICK_ID, RATE, 1100);
        for (int i = 1; i <= 12; i++ ){
            bank.getAccounts().get(QUICK_ID).passMonth();
        }

        command = String.format("transfer %s %s %f", QUICK_ID, EXIST_ID, 1100.00);
        subCommands = command.split(" ");
        transferComProcessor.processCommand(subCommands);

        assertTwoAcctBalances(QUICK_ID, 0, EXIST_ID, 1500.00);
    }

    @Test
    void requested_transfer_amount_greater_than_acct_balance_() {
        bank.addSavings(QUICK_ID, RATE);

        command = String.format("transfer %s %s %f", QUICK_ID, EXIST_ID, 500.00);
        subCommands = command.split(" ");
        transferComProcessor.processCommand(subCommands);

        assertTwoAcctBalances(QUICK_ID, 0, EXIST_ID, 400.00);
    }

}
