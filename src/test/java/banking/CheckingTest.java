package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckingTest {

    public static final String ID = "12345678";
    Checking checking;

    @BeforeEach
    void setUp() {
        checking = new Checking(ID);
        checking.newRate(0.6);
    }

    @Test
    void checking_has_ID() {
        assertEquals("12345678", checking.getId());
    }

    @Test
    void assign_checking_rate() {
        assertEquals(0.6, checking.getRate());
    }

    @Test
    void checking_has_initial_balance_of_zero() {
        assertEquals(0, checking.getBalance());
    }

    @Test
    void get_max_deposit_checking() {
        assertEquals(1000, checking.getMaxDeposit());
    }

    @Test
    void get_max_withdraw_checking() {
        assertEquals(400, checking.getMaxWithdraw());
    }
}
