package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PassTimeTest extends CommandTestingPackage{
    PassTime passTime;

    @BeforeEach
    void setUp(){
        bank = new Bank();
        passTime = new PassTime(bank);
        bank.addChecking(EXIST_ID, RATE);
        bank.depositToAcct(EXIST_ID, 400);
    }

    @Test
    void account_age_is_1_after_one_month(){
        passTime.pass(1);
        Account account = bank.getAcctFromBank(EXIST_ID);
        assertEquals(1, account.getAcctAge());
    }

    @Test
    void account_age_is_60_after_60_month(){
        passTime.pass(60);
        Account account = bank.getAcctFromBank(EXIST_ID);
        assertEquals(60, account.getAcctAge());
    }

    @Test
    void account_age_is_6_after_six_months(){
        passTime.pass(6);
        Account account = bank.getAcctFromBank(EXIST_ID);
        assertEquals(6, account.getAcctAge());
    }

    @Test
    void two_accounts_age_for_both_is_1_after_one_month(){
        bank.addSavings(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 1000);
        passTime.pass(1);

        Account acct1 = bank.getAcctFromBank(EXIST_ID);
        Account acct2 = bank.getAcctFromBank(QUICK_ID);
        assertEquals(1, acct1.getAcctAge());
        assertEquals(1, acct2.getAcctAge());
    }

    @Test
    void two_accounts_age_for_both_is_12_after_12_months(){
        bank.addCD(QUICK_ID, RATE, 1000);
        passTime.pass(12);

        Account acct1 = bank.getAcctFromBank(EXIST_ID);
        Account acct2 = bank.getAcctFromBank(QUICK_ID);
        assertEquals(12, acct1.getAcctAge());
        assertEquals(12, acct2.getAcctAge());
    }

    @Test
    void account_balance_is_0_delete_account(){
        bank.addChecking(QUICK_ID, RATE);

        passTime.pass(1);
        actual = bank.accountExistByQuickID(QUICK_ID);
        assertFalse(actual);
    }

    @Test
    void two_account_balances_are_0_delete_both_account(){
        bank.addChecking(QUICK_ID, RATE);
        bank.addSavings("12341234", RATE);

        passTime.pass(1);
        boolean a1 = bank.accountExistByQuickID(QUICK_ID);
        boolean a2 = bank.accountExistByQuickID("12341234");
        actual = !a1 && !a2 ;
        assertTrue(actual);
    }

    @Test
    void account_balance_is_100_does_not_incur_fee(){
        bank.addChecking(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 100);
        passTime.pass(1);

        Account account = bank.getAcctFromBank(QUICK_ID);
        assertEquals(100.05, round2dec(account.getBalance()) );
    }

    @Test
    void account_balance_is_less_than_100_withdraw_25(){
        bank.addChecking(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 50);
        passTime.pass(1);

        Account account = bank.getAcctFromBank(QUICK_ID);
        assertEquals(25.01, round2dec(account.getBalance()) );
    }

    @Test
    void two_account_balances_are_less_than_100_withdraw_25_from_both(){
        bank.addChecking(QUICK_ID, RATE);
        bank.depositToAcct(QUICK_ID, 50);
        bank.addChecking("12341234", RATE);
        bank.depositToAcct("12341234", 75);

        passTime.pass(1);
        boolean a1 = 25.01 == round2dec(bank.getAcctFromBank(QUICK_ID).getBalance() );
        boolean a2 = 50.03 == round2dec(bank.getAcctFromBank("12341234").getBalance() );
        assertTrue(a1 && a2);
    }

    @Test
    void passTime_deposits_interest_from_calculateAPR(){
        passTime.pass(1);
        assertEquals(400.2, bank.getAcctFromBank(EXIST_ID).getBalance());

    }

    @Test
    void passTime_deposits_interest_from_calculateAPR_for_cd(){
        bank.addCD(QUICK_ID, RATE,1000);
        passTime.pass(1);
        Account account = bank.getAcctFromBank(QUICK_ID);
        assertEquals(1002.00, round2dec(account.getBalance()));
    }
}
