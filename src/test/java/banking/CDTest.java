package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CDTest {

    public static final String ID = "12345678";
    CD cd;

    @BeforeEach
    void setUp() {
        cd = new CD(ID, 0.6, 1000);
    }

    @Test
    void cd_has_ID() {
        assertEquals("12345678", cd.getId());
    }

    @Test
    void cd_instantiated_with_rate() {
        assertEquals(0.6, cd.getRate());
    }

    @Test
    void cd_has_initial_balance_of_1000() {
        assertEquals(1000, cd.getBalance());
    }


    @Test
    void get_minimum_required_creation_amount() {
        assertEquals(1000, cd.getMinCreationAmount());
    }

    @Test
    void get_max_deposit_cd() {
        assertEquals(0, cd.getMaxDeposit());
    }

}
